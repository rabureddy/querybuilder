package com.example.reav.POJOs;

import java.util.ArrayList;

public class QueryBuilder
{
    private String queryString;

    private String count;

    private String eventQueryString;

    private Boolean isChecked;

    private String selectedNumber;

    private String showQuery;

    private String selectedEvent;

    private ArrayList<Rules> rules;

    private Rules[] eventRules;

    private String selectedVariable;

    private String operator;

    private String selectedRange;

    private ApplyIndexDateToDataset applyIndexDate;

    private int minOccurrenceCountGroup;

    private String minOccurrenceOperationGroup;

    public String getQueryString ()
    {
        return queryString;
    }

    public void setQueryString (String queryString)
    {
        this.queryString = queryString;
    }

    public String getCount ()
    {
        return count;
    }

    public void setCount (String count)
    {
        this.count = count;
    }

    public String getEventQueryString ()
    {
        return eventQueryString;
    }

    public void setEventQueryString (String eventQueryString)
    {
        this.eventQueryString = eventQueryString;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }

    public String getSelectedNumber ()
    {
        return selectedNumber;
    }

    public void setSelectedNumber (String selectedNumber)
    {
        this.selectedNumber = selectedNumber;
    }

    public String getShowQuery ()
    {
        return showQuery;
    }

    public void setShowQuery (String showQuery)
    {
        this.showQuery = showQuery;
    }

    public String getSelectedEvent ()
    {
        return selectedEvent;
    }

    public void setSelectedEvent (String selectedEvent)
    {
        this.selectedEvent = selectedEvent;
    }

    public ArrayList<Rules> getRules() {
        return rules;
    }

    public void setRules(ArrayList<Rules> rules) {
        this.rules = rules;
    }

    public Rules[] getEventRules() {
        return eventRules;
    }

    public void setEventRules(Rules[] eventRules) {
        this.eventRules = eventRules;
    }

    public String getSelectedVariable ()
    {
        return selectedVariable;
    }

    public void setSelectedVariable (String selectedVariable)
    {
        this.selectedVariable = selectedVariable;
    }

    public String getOperator ()
    {
        return operator;
    }

    public void setOperator (String operator)
    {
        this.operator = operator;
    }

    public String getSelectedRange ()
    {
        return selectedRange;
    }

    public void setSelectedRange (String selectedRange)
    {
        this.selectedRange = selectedRange;
    }

    public ApplyIndexDateToDataset getApplyIndexDate() {
        return applyIndexDate;
    }

    public void setApplyIndexDate(ApplyIndexDateToDataset applyIndexDate) {
        this.applyIndexDate = applyIndexDate;
    }

    public int getMinOccurrenceCountGroup() {
        return minOccurrenceCountGroup;
    }

    public void setMinOccurrenceCountGroup(int minOccurrenceCountGroup) {
        this.minOccurrenceCountGroup = minOccurrenceCountGroup;
    }

    public String getMinOccurrenceOperationGroup() {
        return minOccurrenceOperationGroup;
    }

    public void setMinOccurrenceOperationGroup(String minOccurrenceOperationGroup) {
        this.minOccurrenceOperationGroup = minOccurrenceOperationGroup;
    }

    /*@Override
    public String toString()
    {
        return "ClassPojo [queryString = "+queryString+", count = "+count+", eventQueryString = "+eventQueryString+", isChecked = "+isChecked+", selectedNumber = "+selectedNumber+", showQuery = "+showQuery+", selectedEvent = "+selectedEvent+", rules = "+rules+", selectedVariable = "+selectedVariable+", operator = "+operator+", selectedRange = "+selectedRange+"]";
    }*/
}
