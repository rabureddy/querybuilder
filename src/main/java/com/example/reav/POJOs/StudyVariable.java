package com.example.reav.POJOs;

public class StudyVariable {
    // criteria, uniqueID, jsonToFilter, dateFormat
    private String variableValueAlias;

    private String descriptiveFunction;

    private String raVariable;

    private String itemName;

    private String isPrimary;

    private String criteria;

    private String variableName;

    private String rawTableName;

    private String type;

    private String variableAlias;

    private String uniqueId;

    private String id;

    private String[] jsonToFilter;

    private String category;

    private String dataType;

    private String statType;

    private Dataset dataset;

    private String datasetName;

    private String description;

    private String dateFormat;

    private String creationTimestamp;

    private String variable;

    public String getVariableValueAlias() {
        return variableValueAlias;
    }

    public void setVariableValueAlias(String variableValueAlias) {
        this.variableValueAlias = variableValueAlias;
    }

    public String getDescriptiveFunction() {
        return descriptiveFunction;
    }

    public void setDescriptiveFunction(String descriptiveFunction) {
        this.descriptiveFunction = descriptiveFunction;
    }

    public String getRaVariable() {
        return raVariable;
    }

    public void setRaVariable(String raVariable) {
        this.raVariable = raVariable;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(String isPrimary) {
        this.isPrimary = isPrimary;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public String getVariableName() {
        return variableName;
    }

    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    public String getRawTableName() {
        return rawTableName;
    }

    public void setRawTableName(String rawTableName) {
        this.rawTableName = rawTableName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVariableAlias() {
        return variableAlias;
    }

    public void setVariableAlias(String variableAlias) {
        this.variableAlias = variableAlias;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getJsonToFilter() {
        return jsonToFilter;
    }

    public void setJsonToFilter(String[] jsonToFilter) {
        this.jsonToFilter = jsonToFilter;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getStatType() {
        return statType;
    }

    public void setStatType(String statType) {
        this.statType = statType;
    }

    public Dataset getDataset() {
        return dataset;
    }

    public void setDataset(Dataset dataset) {
        this.dataset = dataset;
    }

    public String getDatasetName() {
        return datasetName;
    }

    public void setDatasetName(String datasetName) {
        this.datasetName = datasetName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateFormat() {
        return dateFormat;
    }

    public void setDateFormat(String dateFormat) {
        this.dateFormat = dateFormat;
    }

    public String getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(String creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    /*@Override
    public String toString() {
        return "ClassPojo [variableValueAlias = " + variableValueAlias + ", descriptiveFunction = " + descriptiveFunction + ", raVariable = " + raVariable + ", itemName = " + itemName + ", isPrimary = " + isPrimary + ", criteria = " + criteria + ", variableName = " + variableName + ", rawTableName = " + rawTableName + ", type = " + type + ", variableAlias = " + variableAlias + ", uniqueId = " + uniqueId + ", id = " + id + ", jsonToFilter = " + jsonToFilter + ", category = " + category + ", dataType = " + dataType + ", statType = " + statType + ", dataset = " + dataset + ", datasetName = " + datasetName + ", description = " + description + ", dateFormat = " + dateFormat + ", creationTimestamp = " + creationTimestamp + ", variable = " + variable + "]";
    }*/
}