package com.example.reav.POJOs;

public class Data
{
    private String isSelected;

    private String name;

    private String value;

    public String getIsSelected ()
    {
        return isSelected;
    }

    public void setIsSelected (String isSelected)
    {
        this.isSelected = isSelected;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getValue ()
    {
        return value;
    }

    public void setValue (String value)
    {
        this.value = value;
    }

    /*@Override
    public String toString()
    {
        return "ClassPojo [isSelected = "+isSelected+", name = "+name+", value = "+value+"]";
    }
    */
}