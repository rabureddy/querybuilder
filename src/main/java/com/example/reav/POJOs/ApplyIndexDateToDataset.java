package com.example.reav.POJOs;

public class ApplyIndexDateToDataset {

    private String beforeOperator;

    private int beforeNDays;

    private String afterOperator;

    private int afterNDays;

    private int betweenNDays;

    private String dateColumn;

    public int getBeforeNDays() {
        return beforeNDays;
    }

    public void setBeforeNDays(int beforeNDays) {
        this.beforeNDays = beforeNDays;
    }

    public int getAfterNDays() {
        return afterNDays;
    }

    public void setAfterNDays(int afterNDays) {
        this.afterNDays = afterNDays;
    }

    public int getBetweenNDays() {
        return betweenNDays;
    }

    public void setBetweenNDays(int betweenNDays) {
        this.betweenNDays = betweenNDays;
    }


    public String getBeforeOperator() {
        return beforeOperator;
    }

    public void setBeforeOperator(String beforeOperator) {
        this.beforeOperator = beforeOperator;
    }

    public String getAfterOperator() {
        return afterOperator;
    }

    public void setAfterOperator(String afterOperator) {
        this.afterOperator = afterOperator;
    }

    public String getDateColumn() {
        return dateColumn;
    }

    public void setDateColumn(String dateColumn) {
        this.dateColumn = dateColumn;
    }
}
