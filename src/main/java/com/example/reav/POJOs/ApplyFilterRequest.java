package com.example.reav.POJOs;

import java.util.ArrayList;

public class ApplyFilterRequest {

    private ArrayList<QueryBuilder> conditions;
    private ArrayList<Reference> references;

    public ArrayList<QueryBuilder> getConditions() {
        return conditions;
    }

    public void setConditions(ArrayList<QueryBuilder> conditions) {
        this.conditions = conditions;
    }

    public ArrayList<Reference> getReferences() {
        return references;
    }

    public void setReferences(ArrayList<Reference> references) {
        this.references = references;
    }
}
