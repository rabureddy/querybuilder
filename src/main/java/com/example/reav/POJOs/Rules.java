package com.example.reav.POJOs;

public class Rules
{
    private StudyVariable[] field;

    public StudyVariable[] previousGroupEventField;

    private String queryString;

    private String text;

    private String count;

    private String condition;

    private String datasetName;

    private Data[] data;

    private String datasetNameraworwork;

    private String operator;

    private int minOccurrenceCount;

    private String minOccurrenceOperation;

    public StudyVariable[] getField ()
    {
        return field;
    }

    public void setField (StudyVariable[] field)
    {
        this.field = field;
    }

    public StudyVariable[] getPreviousGroupEventField() {
        return previousGroupEventField;
    }

    public void setPreviousGroupEventField(StudyVariable[] previousGroupEventField) {
        this.previousGroupEventField = previousGroupEventField;
    }

    public String getQueryString ()
    {
        return queryString;
    }

    public void setQueryString (String queryString)
    {
        this.queryString = queryString;
    }

    public String getText ()
    {
        return text;
    }

    public void setText (String text)
    {
        this.text = text;
    }

    public String getCount ()
    {
        return count;
    }

    public void setCount (String count)
    {
        this.count = count;
    }

    public String getCondition ()
    {
        return condition;
    }

    public void setCondition (String condition)
    {
        this.condition = condition;
    }

    public String getDatasetName ()
    {
        return datasetName;
    }

    public void setDatasetName (String datasetName)
    {
        this.datasetName = datasetName;
    }

    public Data[] getData ()
    {
        return data;
    }

    public void setData (Data[] data)
    {
        this.data = data;
    }

    public String getDatasetNameraworwork ()
    {
        return datasetNameraworwork;
    }

    public int getMinOccurrenceCount() {
        return minOccurrenceCount;
    }

    public void setMinOccurrenceCount(int minOccurrenceCount) {
        this.minOccurrenceCount = minOccurrenceCount;
    }

    public void setDatasetNameraworwork (String datasetNameraworwork)
    {
        this.datasetNameraworwork = datasetNameraworwork;
    }

    public String getOperator ()
    {
        return operator;
    }

    public void setOperator (String operator)
    {
        this.operator = operator;
    }

    public String getMinOccurrenceOperation() {
        return minOccurrenceOperation;
    }

    public void setMinOccurrenceOperation(String minOccurrenceOperation) {
        this.minOccurrenceOperation = minOccurrenceOperation;
    }

    /*@Override
    public String toString()
    {
        return "ClassPojo [field = "+field+", queryString = "+queryString+", text = "+text+", count = "+count+", condition = "+condition+", datasetName = "+datasetName+", data = "+data+", datasetNameraworwork = "+datasetNameraworwork+", operator = "+operator+"]";
    }*/
}