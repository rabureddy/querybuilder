package com.example.reav;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReavApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReavApplication.class, args);
    }

}

