package com.example.reav;

import com.example.reav.POJOs.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class QueryBuilderToSQL {

    //@Autowired
    //DatasetPostScriptRepository datasetPostScriptRepository;

    public String generateSimpleQueryBuilderSqlString(ArrayList<QueryBuilder> qbRequest, String prefix,
                                                      Boolean wantCounts, ArrayList<Reference> references,
                                                      String dateColumnName) {

        ArrayList<String> allSQLStatements = new ArrayList<>();
        String patientIdVariable = "desynpuf_id";
        StringBuilder completeSQLString = new StringBuilder();
        String unionCompleteString = "";

        if (references == null || references.size() <= 1) {
            String tableName = prefix + qbRequest.get(0).getRules().get(0).getField()[0].getRawTableName();
            unionCompleteString = generateQueryBuilderSqlString(qbRequest, tableName, wantCounts);
        } else {
            int itr = 0;
            for (Reference r : references) {
                //dateColumnName = datasetPostScriptRepository.findByVariableByDatasetId(r.getTableId()).getVariableName();
                qbRequest.get(0).getApplyIndexDate().setDateColumn(dateColumnName);
                String tableName = prefix + r.getTableName();

                if (itr > 0) {
                    // Apply Union here
                    completeSQLString.append("\n UNION ALL \n").append(generateQueryBuilderSqlString(qbRequest, tableName, wantCounts));

                } else {
                    completeSQLString = new StringBuilder(generateQueryBuilderSqlString(qbRequest, tableName, wantCounts));
                }
                itr++;
            }

            unionCompleteString = "select " + patientIdVariable + " from ( " + completeSQLString.toString() + " ) as mRef";

        }

        return unionCompleteString;
    }

    private String eachValue(String value, Boolean toAppend) {

        if (toAppend) {
            value = "'" + value + "'";
        }

        return value;
    }

    public String generateQueryBuilderSqlString(ArrayList<QueryBuilder> qbRequest,
                                                String stage, Boolean wantCounts) {
        String patientIdVariable = "desynpuf_id";
        int currentGroupIndex = 0;
        int currentRuleIndex = 0;
        int currentFieldIndex = 0;
        int currentDataValueIndex = 0;
        int currentEventRuleIndex = 0;

        String selectedGroupColumns = "distinct( " + patientIdVariable + " )";


        Boolean isSimpleQuery = true;

        String completeQueryString = "";

        for (QueryBuilder group : qbRequest) {

            String eachGroupSql = "";
            String allRulesSql = "";

            currentRuleIndex = 0;
            isSimpleQuery = toUseJoins(qbRequest);
            System.out.println("Use Comp: " + isSimpleQuery);

            if (!isSimpleQuery) {

                if (group.getChecked() != null && group.getChecked()) {


                } else {
                    //Fetch all column names to extract for Select (*) statement
                    for (Rules eachRule : group.getRules()) {

                        String selectedRuleColumns = "distinct( " + patientIdVariable + " )";

                        //each ruleSql = variableName + condition + value
                        String eachRuleSql = "";
                        if (eachRule.getField() != null && eachRule.getField().length > 0
                                && eachRule.getData() != null && eachRule.getData().length > 0) {
                            String multiBaseRule = "";
                            for (int fieldVarIndex = 0; fieldVarIndex < eachRule.getField().length; fieldVarIndex++) {


                                StudyVariable variable = eachRule.getField()[fieldVarIndex];
                                String variableName = eachRule.getField()[fieldVarIndex].getVariableName();

                                selectedRuleColumns = selectedRuleColumns
                                        + (selectedRuleColumns.contains(variableName.trim()) ? "" : ", " + variableName);

                                //Keep appending this baseRule to multiple data values.
                                //Ex: Race =
                                String baseRuleSql = "";

                                baseRuleSql = variableName + " " + eachRule.getCondition().trim() + " ";
                                System.out.println("Base Rule: " + baseRuleSql);


                                //Iterate over each value
                                //Ex: Race = 1, 2, 3 values.
                                String allValuesSql = "";
                                currentDataValueIndex = 0;
                                for (Data eachSelectedValue : eachRule.getData()) {
                                    String eachValueSql = "";

                                    //Check for String or Double and append " " accordingly.
                                    //Perform AND OR UNION INTERSECTION HERE.
                                    if (variable.getDataType().trim().equalsIgnoreCase("string")
                                            || variable.getDataType().trim().equalsIgnoreCase("date")) {
                                        //Use Rule operator
                                        //Based on Sai's request and requirement.
                                        eachValueSql = (currentDataValueIndex > 0 ? " Or " + /*group.getRules().get(0).getOperator() +*/ " " : "") +//eachRule.getOperator().trim() + " " : "") +
                                                baseRuleSql + "\"" + eachSelectedValue.getValue().trim() + "\"";
                                    } else {
                                        //Use Rule operator
                                        eachValueSql = (currentDataValueIndex > 0 ? " " + group.getRules().get(0).getOperator() + " " : "") //eachRule.getOperator().trim() + " " : "") +
                                                + baseRuleSql + eachSelectedValue.getValue().trim();
                                    }

                                    //For adding multiple or conditions for multiple fields in one variable selection.
                                    multiBaseRule = multiBaseRule + ((fieldVarIndex > 0 && currentDataValueIndex == 0) ? " OR " : " ") + eachValueSql;
                                    eachValueSql = multiBaseRule;

                                    //Preparing the Rules Statement.
                                    //BENE_SEX_IDENT_CD  = "1" + OR BENE_SEX_IDENT_CD  = "2"
                                    allValuesSql = (currentDataValueIndex == eachRule.getData().length) ? allValuesSql + eachValueSql : eachValueSql;
                                    if (!isSimpleQuery) {
                                        //TODO get PatientID variable by datasetId and datasetName
                                        //allValuesSql = "select **" + selectedRuleColumns + " from " + stage
                                        allValuesSql = "select * from "
                                                + stage + eachRule.getField()[0].getRawTableName() +
                                                " where " + allValuesSql;
                                        if (eachRule.getMinOccurrenceCount() > 1) {
                                            allValuesSql = allValuesSql.replace("*", patientIdVariable);
                                            allValuesSql = allValuesSql + "\n GROUP BY " + patientIdVariable + " HAVING COUNT(" + patientIdVariable + ") " +
                                                    eachRule.getMinOccurrenceOperation() + " " + eachRule.getMinOccurrenceCount();
                                        }
                                    }
                                    currentDataValueIndex++;
                                }
                                //Prepare all rules with reference inside group
                                //( BENE_SEX_IDENT_CD  = "1" OR BENE_SEX_IDENT_CD  = "2" ) t0
                                //System.out.println(group.getRules().length > currentRuleIndex);

                                eachRuleSql = ((!isSimpleQuery && currentRuleIndex == 0) ? "SELECt "
                                        + " t" + currentRuleIndex + "." + ((currentRuleIndex < group.getRules().size() - 1
                                        && group.getRules().get(currentRuleIndex + 1).getOperator().trim().equalsIgnoreCase("OR")
                                        //|| eachRule.getOperator().trim().equalsIgnoreCase("OR"))
                                )
                                        ? patientIdVariable + " " : "* ")//
                                        + " FROM (" : "(") + allValuesSql + ") ";
                            }

                        } else {
                            return "";
                        }

                        //Add any operator between rules
                        if (currentRuleIndex > 0 && eachRule.getOperator().trim().equalsIgnoreCase("AND")
                                && !isSimpleQuery) {
                            allRulesSql = allRulesSql +
                                    "\n INNER JOIN \n" +
                                    //
                                    eachRuleSql + " t" + currentRuleIndex +
                                    //ON t0.desynpuf_id = t1.desynpuf_id
                                    "\n ON t" + (currentRuleIndex - 1) + "." + patientIdVariable + " = t" + currentRuleIndex + "." + patientIdVariable;

                        } else if (currentRuleIndex > 0 && eachRule.getOperator().trim().equalsIgnoreCase("OR")
                                && !isSimpleQuery) {
                            allRulesSql = allRulesSql +
                                    "\n " + "UNION ALL" + " \n" +
                                    "SELECT t" + currentRuleIndex + "." + patientIdVariable + " FROM " + eachRuleSql + "t" + currentRuleIndex;
                        } else {
                            allRulesSql = allRulesSql +
                                    ((currentRuleIndex > 0) ? " " + eachRule.getOperator().trim() + " " : "") +
                                    eachRuleSql + (!isSimpleQuery ? " t" + currentRuleIndex : "");
                        }

                        currentRuleIndex++;
                    }
                }
                //Check if we have to apply AND or OR between
                if (currentGroupIndex > 0 && group.getOperator().trim().equalsIgnoreCase("AND")
                        && !isSimpleQuery) {
                    completeQueryString = completeQueryString
                            + "\n INNER JOIN \n"
                            + "(" + allRulesSql + ") "
                            + "gRef" + currentGroupIndex + " "
                            //gRef0.desynpuf_id = gRef2.desynpuf_id
                            + "\n ON gRef" + (currentGroupIndex - 1) + "." + patientIdVariable + " = gRef" + currentGroupIndex + "." + patientIdVariable;

                    //((currentGroupIndex > 0)? " ) "+ : "" ) + allRulesSql +" )" ;
                } else if (currentRuleIndex > 0 && group.getOperator().trim().equalsIgnoreCase("OR")
                        && !isSimpleQuery && qbRequest.size() > 1 && currentGroupIndex != 0) {

                    completeQueryString =
                            (currentGroupIndex + 1 == qbRequest.size() ? ""
                                    : " (SELeCT gRef" + (currentGroupIndex - 1) + "." + patientIdVariable + " From ")
                                    + completeQueryString
                                    + "\n UNION DISTINCT \n" +
                                    "(" + allRulesSql.replace("*", patientIdVariable) + ")" + (currentGroupIndex + 1 == qbRequest.size() ? "" : ") gRef" + currentGroupIndex)
                                    + " ";
                    //((currentGroupIndex > 0)? " ) "+ : "" ) + allRulesSql +" )" ;
                } else {
                    completeQueryString = "(" + completeQueryString + allRulesSql + ")"
                            + (!isSimpleQuery ? " gRef" + currentGroupIndex : "");
                }

                if (group.getEventRules() != null
                        && group.getEventRules().length > 0 && group.getEventRules()[0].getField().length > 0) {
                    currentEventRuleIndex = 0;
                    for (Rules eventRule : group.getEventRules()) {
                        //myEvent =
                        //Left operand
                        //Gets appended before the Inner join. Do not want this if only one group is present.
                        completeQueryString = completeQueryString
                                + (currentGroupIndex > 0 ? " " + eventRule.getOperator() + " " : "");
                        //For right operand.
                        if (currentGroupIndex > 0 && eventRule.getField()[0].getDataType().trim().equalsIgnoreCase("string")) {
                            completeQueryString = completeQueryString + "\"" + eventRule.getData()[0].getValue() + "\"";

                        } else if (currentGroupIndex > 0 && eventRule.getField()[0].getDataType().trim().equalsIgnoreCase("column")) {
                            //gRef0.CLM_FROM_DT > gRef1.CLM_FROM_DT
                            completeQueryString = completeQueryString + " gRef" + (currentGroupIndex - 1) + "." +
                                    eventRule.getPreviousGroupEventField()[0].getVariableName()
                                    + " " + eventRule.getCondition() + " " + "gRef" + (currentGroupIndex) + "." +
                                    eventRule.getField()[0].getVariableName() + "";
                        } else if (currentGroupIndex > 0) {
                            //datediff( gRef1.CLM_FROM_DT, gRef0.CLM_FROM_DT) <= 20)
                            completeQueryString = completeQueryString + " datediff( gRef" + (currentGroupIndex - 1) + "." +
                                    eventRule.getPreviousGroupEventField()[0].getVariableName()
                                    + ", " + "gRef" + (currentGroupIndex) + "." +
                                    eventRule.getField()[0].getVariableName()
                                    + ") " + eventRule.getCondition() + " " + eventRule.getData()[0].getValue();
                        }
                    }
                }

            /*if (currentGroupIndex > 0) {
                completeQueryString = completeQueryString + group.getOperator().trim() + "(" + allRulesSql + ")";
                //((currentGroupIndex > 0)? " ) "+ : "" ) + allRulesSql +" )" ;
            } else {
                completeQueryString = "(" + completeQueryString + allRulesSql + ")";
            }*/

                currentGroupIndex++;
            } else {

                String tableName;
                if (stage.length() <= "sty_999__".length()) {
                    tableName = stage + qbRequest.get(0).getRules().get(0).getField()[0].getRawTableName();
                } else {
                    tableName = stage;
                }
                //Does not support Group.
                completeQueryString = generateSimpleWhereCondition(group, true, patientIdVariable, tableName);
            }

        }

        if (isSimpleQuery && !completeQueryString.contains(" t0")) {
            String tableName = "";
            if (stage.length() <= "sty_999__".length()) {
                tableName = stage + qbRequest.get(0).getRules().get(0).getField()[0].getRawTableName();
            } else {
                tableName = stage;
            }
            completeQueryString = "SELECT " +
                    patientIdVariable +
                    " from " +

                    tableName + //wrk_rawTableName
                    "\n where \n" + completeQueryString;
        } else if (!isSimpleQuery) {
            //Add gRef references at the beginning.
            completeQueryString = "\nSELECT "
                    + "gRef" + ((currentGroupIndex == 0 || currentGroupIndex == 1) ? (currentGroupIndex - 1) : currentGroupIndex - 2)
                    //+ "." + (qbRequest.get(currentGroupIndex-1).getOperator()?:)
                    + "." + patientIdVariable
                    + " from \n" + completeQueryString;
        }
        completeQueryString = "SELECT " + (wantCounts ? " count(distinct " : "") + " fRef." + patientIdVariable
                + (wantCounts ? ") as totalCount " : "") + " from (" + completeQueryString + ") fRef";

        return completeQueryString;
    }


    private String generateSimpleWhereCondition(QueryBuilder qb, Boolean wantCounts, String patientIdVariable, String tableName) {

        StringBuilder allRules = new StringBuilder();
        //Each Group has multiple rules.
        for (int i = 0; i < qb.getRules().size(); i++) {
            Rules r = qb.getRules().get(i);
            //Each rule has multiple variables in fields
            String eachRule = "";
            int fieldCounter = 0;
            for (StudyVariable f : r.getField()) {
                String eachField = "";
                String variableName = f.getVariableName();
                //Use IN only for many values or else use =
                Boolean appendQuotes = false;
                appendQuotes = f.getDataType().trim().equalsIgnoreCase("STRING");
                if (r.getData().length > 1) {
                    StringBuilder allDataValues = new StringBuilder();

                    if (r.getCondition().equals("<*>")) {

                        for (Data d : r.getData()) {

                            allDataValues.append(eachValue(d.getValue(), appendQuotes)).append(" AND " + variableName + " < ");

                        }
                        eachField = variableName + " > " + allDataValues.substring(0, allDataValues.length() - (variableName.length() + 8));

                    } else {
                        for (Data d : r.getData()) {
                            allDataValues.append(eachValue(d.getValue(), appendQuotes)).append(", ");
                        }
                        eachField = variableName + " IN (" + allDataValues.substring(0, allDataValues.length() - 2) + ") ";
                    }
                } else {
                    //Check if value is of type string or date.
                    //It could be a column too.
                    eachField = variableName + " " + r.getCondition() + " " +
                            eachValue(r.getData()[0].getValue(), appendQuotes) + " ";
                }

                if (r.getMinOccurrenceCount() > 1 && fieldCounter + 1 == r.getField().length) {
                    eachField = eachField + "\n GROUP BY " + patientIdVariable + " HAVING COUNT(" + patientIdVariable + ") " +
                            r.getMinOccurrenceOperation() + " " + r.getMinOccurrenceCount();
                }

                // Replace trialing " OR "
                if (fieldCounter + 1 != r.getField().length) {
                    eachRule = eachRule + eachField + " OR ";
                } else {
                    eachRule = eachRule + eachField;

                }

                fieldCounter++;

            }

            if (i + 1 == qb.getRules().size()) {
                allRules.append(eachRule).append(" ");
            } else {

                //Apply Next Rule's operator.
                Rules nextRule = qb.getRules().get(i + 1);
                allRules.append(eachRule).append(" ").append(nextRule.getOperator()).append(" ");
            }


        }

        // below is for obtaining counts.
        String beforeClause = "";
        String afterClause = "";
        ApplyIndexDateToDataset applyIndexDate = qb.getApplyIndexDate();
        if (applyIndexDate != null && applyIndexDate.getDateColumn() != null && !applyIndexDate.getDateColumn().isEmpty() && wantCounts) {
            String currentDateColumn = applyIndexDate.getDateColumn();
            if (applyIndexDate.getBeforeOperator() != null && !applyIndexDate.getBeforeOperator().isEmpty()) {
                beforeClause = " AND datediff(index_date, " + currentDateColumn + ") " + applyIndexDate.getBeforeOperator() +
                        applyIndexDate.getBeforeNDays() + " ";
                if (applyIndexDate.getAfterOperator() != null && applyIndexDate.getAfterOperator().isEmpty())
                    allRules.append(beforeClause);
            }

            // After Index date means, the the date in working table is greater than index_date
            // Apply this only if before is empty.
            if (applyIndexDate.getAfterOperator() != null && !applyIndexDate.getAfterOperator().isEmpty()) {
                afterClause = " AND datediff(" + currentDateColumn + ", index_date ) " + applyIndexDate.getAfterOperator() +
                        applyIndexDate.getAfterNDays() + " ";
                // Append after only if the before is empty
                if (applyIndexDate.getBeforeOperator() != null && applyIndexDate.getBeforeOperator().isEmpty())
                    allRules.append(afterClause);
            }

            if ((applyIndexDate.getBeforeOperator() != null && !applyIndexDate.getBeforeOperator().isEmpty()) &&
                    (applyIndexDate.getAfterOperator() != null && !applyIndexDate.getAfterOperator().isEmpty())) {


                /*
                Select  distinct t0.desynpuf_id from (select desynpuf_id from wrk_349_DE1_0_2008_to_2010_Outpatient_Claims_Sample_1 t1  where age > 30   AND datediff(index_date, clm_from_dt) >20 ) t0,
                    (select desynpuf_id from wrk_349_DE1_0_2008_to_2010_Outpatient_Claims_Sample_1 where age > 30   AND datediff(index_date, clm_from_dt) <20) t1 WHERE t0.desynpuf_id = t1.desynpuf_id;
                 */
                // SELF JOIN
                String preTable = "(select " + patientIdVariable + " from " + tableName + "  " + " where ";
                StringBuilder allRulesWithBetween = new StringBuilder();
                allRulesWithBetween.append("Select  distinct t0.").append(patientIdVariable).append(" from ").append(preTable);
                allRulesWithBetween.append(allRules.toString()).append(beforeClause);
                allRulesWithBetween.append(" ) t0, ");
                allRulesWithBetween.append(preTable).append(allRules.toString()).append(afterClause);
                allRulesWithBetween.append(" ) t1 WHERE t0.").append(patientIdVariable).append(" = t1.").append(patientIdVariable);
                //allRules.append(allRules.toString());
                allRules = allRulesWithBetween;
            }


        }


        return allRules.toString();
    }


    public Boolean toUseJoins(ArrayList<QueryBuilder> qbRequest) {

        if (qbRequest.size() > 1)
            return false;

        String checkTableName = qbRequest.get(0).getRules().get(0).getField()[0].getRawTableName();
        for (QueryBuilder qb : qbRequest) {

            for (Rules rule : qb.getRules()) {
                if (!rule.getField()[0].getRawTableName().equalsIgnoreCase(checkTableName)) {
                    return false;
                }
            }
        }

        return true;
    }

}
