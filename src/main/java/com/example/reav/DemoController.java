package com.example.reav;

import com.example.reav.POJOs.ApplyFilterRequest;
import com.example.reav.POJOs.QueryBuilder;
import com.example.reav.POJOs.Reference;
import com.example.reav.POJOs.Rules;
import com.example.reav.variables.CreateVariableRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/demo")
public class DemoController {
    @Autowired
    private QueryBuilderToSQL queryBuilderToSQL;

    @Autowired
    private AnalyticsOperationsToSQL analyticsOperationsToSQL;

    @PostMapping(value = "counts/{projectId}/{stateId}/{wantCounts}")
    public ResponseEntity getCohorCount(@RequestBody ArrayList<QueryBuilder> queryBuilder,
                                        @PathVariable Long projectId, @PathVariable Long wantCounts) {
        String completeQuery = queryBuilderToSQL.generateQueryBuilderSqlString(queryBuilder,
                "wrk_" + projectId + "_", (wantCounts == 1));
        System.out.println("Complete Query: " + completeQuery);
        return new ResponseEntity<>(completeQuery, HttpStatus.OK);

    }

    @PostMapping(value = "counts/simple/{projectId}/{stateId}/{wantCounts}")
    public ResponseEntity getSimpleCohorCount(@RequestBody ApplyFilterRequest request,
                                        @PathVariable Long projectId, @PathVariable Long wantCounts) {

        String completeQuery = queryBuilderToSQL.generateSimpleQueryBuilderSqlString(request.getConditions(),
                "wrk_" + projectId + "_", (wantCounts == 1), request.getReferences(), "clm_from_dt");
        System.out.println("Complete Query: " + completeQuery);
        return new ResponseEntity<>(completeQuery, HttpStatus.OK);

    }


    @PostMapping(value = "all-counts/{projectId}/{stateId}/{wantCounts}")
    public ResponseEntity getAllCohorCount(@RequestBody ArrayList<QueryBuilder> queryBuilder,
                                           @PathVariable Long projectId, @PathVariable Long wantCounts) {
        ArrayList<QueryBuilder> childQueries = new ArrayList<>();
        for (QueryBuilder eachGroup : queryBuilder) {
            ArrayList<Rules> toSetRules = new ArrayList();
            int ruleIndex = 0;
            childQueries.add(eachGroup);
            for (Rules eachRule : eachGroup.getRules()) {
                //Make counts calls
                eachRule.setCount(ruleIndex + "");
                toSetRules.add(ruleIndex, eachRule);
                eachGroup.setRules(toSetRules);
                ruleIndex++;
            }
        }
        return new ResponseEntity<>(childQueries, HttpStatus.OK);

    }

    @PostMapping(value = "variable/{projectId}/{stateId}")
    public ResponseEntity createVariable(@RequestBody CreateVariableRequest createVariableRequest) {
        //1. Alter table, add column
        //2. Generate Update Column
        String query = analyticsOperationsToSQL.generateSQL();

        return new ResponseEntity<>(query, HttpStatus.OK);
    }

    @PostMapping(value = "checkpoint/{projectId}/{stateId}")
    public ResponseEntity saveCohortCheckPoint(){

        return new ResponseEntity(HttpStatus.OK);
    }


}
