package com.example.reav.variables;

import com.example.reav.POJOs.QueryBuilder;

import java.util.ArrayList;

/**
 * {
 * "newVariable": "columnName",
 * "type" : "returnType",
 * "tableName": "tableName",
 * "function": "value",
 * "conditions": [],
 * "sourceVariables": "sourcevarlist",
 * "variableAlias": "columnName",
 * "isPrimary": 6
 * }
 */

public class CreateVariableRequest {


    private String newVariable;
    private String variableType;
    //private String tableName;
    private String function;
    private ArrayList<QueryBuilder> queryBuilder;
    private String sourceVariables;
    private String constantToAdd;
    private String variableAlias;
    private int isPrimary;

    public String getNewVariable() {
        return newVariable;
    }

    public void setNewVariable(String newVariable) {
        this.newVariable = newVariable;
    }

    public String getConstantToAdd() {
        return constantToAdd;
    }

    public void setConstantToAdd(String constantToAdd) {
        this.constantToAdd = constantToAdd;
    }

    public String getVariableType() {
        return variableType;
    }

    public void setVariableType(String variableType) {
        this.variableType = variableType;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public ArrayList<QueryBuilder> getQueryBuilder() {
        return queryBuilder;
    }

    public void setQueryBuilder(ArrayList<QueryBuilder> queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    public String getSourceVariables() {
        return sourceVariables;
    }

    public void setSourceVariables(String sourceVariables) {
        this.sourceVariables = sourceVariables;
    }

    public String getVariableAlias() {
        return variableAlias;
    }

    public void setVariableAlias(String variableAlias) {
        this.variableAlias = variableAlias;
    }

    public int getIsPrimary() {
        return isPrimary;
    }

    public void setIsPrimary(int isPrimary) {
        this.isPrimary = isPrimary;
    }
}
